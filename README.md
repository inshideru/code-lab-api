# Code Lab
Laboratório para prática de desenvolvimento Web Service e Banco de Dados.
Demo http://codelab-tisi.rhcloud.com/

##Projeto
Cálculo automatizado de custo e prazo de produção de software.
Para criar o banco de dados do projeto, deverão ser executados os arquivos do diretório  v1 / Core / sql / metricas /
### Frontend:
* AngularJS
* Bootstrap (BootsWatch Flatly Theme)

### Backend:
* PHP 5.3
* Apache Server 2.2
* Banco de Dados PostgreSQL 9.4

## Atenção
### Constantes do sistema
É necessário **criar um arquivo constants.php na pasta v1/Inc** para definir constantes que guardam informações críticas ao sistema.
O arquivo deverá ter o seguinte conteúdo:
```php
<?php
define('DB_HOST', '127.0.0.1');
define('DB_NAME', 'codelab');
define('DB_USER', 'postgres');
define('DB_PASS', '123456');
define('DB_DSN', 'pgsql:host='.DB_HOST.';dbname='.DB_NAME);

// Configurações para envio de e-mail
define('ADMIN_MAIL', '');
define('ADMIN_MAIL_PASS', '');
define('SMTP_MAIL_SERVER', '');

define('APP_NAME', 'Code Lab API');
define('APP_VERSION', '0.0.1');

//define a url do site para ser usada em endereços relativos
define('SITE_URL', str_replace('index.php', '', $_SERVER['PHP_SELF'])); //'http://localhost/dnacrm/'
define('SITE_ROOT', str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME'])); //'C:\htdocs\site\\'
```