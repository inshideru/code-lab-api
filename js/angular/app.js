var codeLabApp = angular.module('codeLabApp',['ngRoute','ngResource', 'ngMessages', 'datatables'])
        .run(function(DTDefaultOptions) {
        DTDefaultOptions.setLanguageSource('js/datatables/js/dataTables.pt-br.lang');
    });

//'js/datatables/js/dataTables.pt-br.lang'

// ROUTES
codeLabApp.config(function($routeProvider) {
    
    $routeProvider
    
        .when('/', {
            templateUrl: 'pages/home.html',
            controller: 'homeController'
        })

        .when('/list', {
            templateUrl: 'pages/list.html',
            controller: 'listController'
        })

        .when('/detail/:id', {
            templateUrl: 'pages/detail.html',
            controller: 'detailController'
        })

        .when('/form/:id', {
            templateUrl: 'pages/form.html',
            controller: 'formController'
        })
    
        .when('/form_message', {
            templateUrl: 'pages/form_message.html',
            controller: 'formMessageController'
        })

        .when('/delete/:id', {
            templateUrl: 'pages/delete_confirm.html',
            controller: 'deleteConfirmController'
        })
});

// SERVICES
codeLabApp.service('projetoService', ['$resource', function($resource) {
    var self = this;

    this.projeto = {};
    
    this.getProjeto = function(id_projeto) {
        var resultProjeto = $resource('/v1/projeto/:id', { callback: "JSON_CALLBACK" }, {get: {method: "JSONP" }});
        return resultProjeto.get({id: id_projeto});
    };
}]);

// CONTROLLERS
codeLabApp.controller('bodyController', ['$scope', function($scope) {
    
    $scope.appName = 'CodeLab';
    
}]);

codeLabApp.controller('homeController', ['$scope', function($scope) {
    
}]);

codeLabApp.controller('listController', ['$scope', '$http', 'DTOptionsBuilder', function($scope, $http, DTOptionsBuilder) {
    
    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDisplayLength(5)
        .withOption('bLengthChange', false)
        .withOption("order", [[ 0, "desc" ]]);

    $scope.projetos = $http.get('v1/projeto')
        .success(function(result) {
            $scope.projetos = result;
    })
        .error(function(data,status) {
            console.log(data);
    });
    
}]);

codeLabApp.controller('detailController', ['$scope', '$routeParams', '$http', function($scope, $routeParams, $http) {
    
    // Métricas
    $http.get('/v1/projeto/' + $routeParams.id + '/metrica')
        .success(function(result) {
            $scope.projeto = result;
    })
        .error(function(data,status) {
        console.log(data);
    });
    
    // Pontos por Função
    // Métricas
    $http.get('/v1/projeto/' + $routeParams.id + '/pontos_funcao')
        .success(function(result) {
            $scope.pontos_funcao = result;
    })
        .error(function(data,status) {
        console.log(data);
    });

}]);

codeLabApp.controller('formController', ['$scope', '$http', '$routeParams', '$location', 'projetoService', function($scope, $http, $routeParams, $location, projetoService) {
    
    $scope.$watch('projeto', function() {
        projetoService.projeto = $scope.projeto;
    });
    
    if ($routeParams.id === 'novo') {
        $scope.projeto = {
            id_projeto: '',
            nome_projeto: 'Novo Projeto',
            modelo: '',
            nivel_influencia: '',
            valor_hora: '',
            id_linguagem: '',
            id_tipo_sistema: ''        
        };
    } else {
        
        $http.get('/v1/projeto/' + $routeParams.id )
            .success(
                function(result) {
                    $scope.projeto = result;
                    $scope.projeto.modelo = $scope.projeto.modelo.substring(1, $scope.projeto.modelo.length - 1);
                    $scope.projeto.valor_hora = accounting.formatNumber($scope.projeto.valor_hora, 2, ".", ",");
                })
            .error(function(data,status) {
                    console.log(data);
                }
            );
        
    };
    
    $http.get('/v1/linguagens')
        .success(
            function(result) {
                $scope.linguagens = result;
            })
        .error(
            function(data, status) {
                console.log(data);
        });
    
    $http.get('v1/tipos_sistema')
        .success(
            function(result) {
                $scope.tipos_sistema = result;
            })
        .error(
            function(data, status) {
                console.log(data);
        });
    
    $scope.gravarProjeto = function() {
        
        if ($scope.projeto.id_projeto === '') {
            
            $http({
                method: 'POST',
                url: '/v1/projeto',
                data: $scope.projeto,
                headers: {'Content-Type': 'application/json; charset=UTF-8'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                }
            })
                .success(function(result){
                    $location.path('/form_message');
            })
                .error(function(data, status){
                    console.log(data);
            });
            
            
        } else {
            
            $http({
                method: 'PUT',
                url: '/v1/projeto/' + $scope.projeto.id_projeto,
                data: $scope.projeto,
                headers: {'Content-Type': 'application/json; charset=UTF-8'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                }
            })
                .success(function(result) {
                    $location.path('/form_message');
                    console.log('Atualizado');
            })
                .error(function(data, status) {
                    console.log(data);
            });                ;
                
        }

    };
    
    //$scope.teste = projetoService.getProjeto(1);
    //console.log($scope.teste);
    
}]);

codeLabApp.controller('formMessageController', ['$scope', 'projetoService', function($scope, projetoService) {
    $scope.projeto = projetoService.projeto;
    $scope.message = 'Projeto ' + $scope.projeto.nome_projeto + ' criado com sucesso!';
    
    console.log($scope.message);    
    
}]);

codeLabApp.controller('deleteConfirmController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {
    $scope.confirmar = false;
    $http.get('/v1/projeto/' + $routeParams.id )
        .success(
        function(result) {
            $scope.projeto = result;
            $scope.message = 'O projeto ' + $scope.projeto.nome_projeto + ' será removido permanentemente!';
        })
        .error(function(data,status) {
            console.log(data);
        }
    );

    $scope.apagarProjeto = function() {

        $http.delete('/v1/projeto/' + $routeParams.id)
            .success(function() {
                $scope.message = $scope.projeto.nome_projeto + ' foi removido!';
            })
            .error(function() {

            });
        $scope.confirmar = true;
    }
}]);