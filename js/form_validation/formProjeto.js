$('#form_projeto').formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        nome_projeto: {
            validators: {

                notEmpty: {
                    message: 'Nome do Projeto é obrigatório.'
                },
                regexp: {
                    regexp: /^[0-9a-zA-ZéúíóáÉÚÍÓÁèùìòàçÇÈÙÌÒÀõãñÕÃÑêûîôâÊÛÎÔÂëÿüïöäËYÜÏÖÄ\-\s]+$/,
                    message: 'O nome do Projeto só pode conter letras, números e espaços.'
                }
            }
        },
        modelo: {
            validators: {
                notEmpty: {
                    message: 'Modelo é obrigatório.'
                },
                regexp: {
                    regexp: /^[,0-9]+$/,
                    message: 'Só pode conter números separados por vígula. Ex: 1,1,1,1'
                }
            }
        },
        nivel_influencia: {
            row: '.col-sm-6',
            validators: {
                notEmpty: {
                    message: 'Nível de Influência é obrigatório.'
                }
            }
        },
        valor_hora: {
            row: '.col-sm-6',
            validators: {
                notEmpty: {
                    message: 'Valor Hora é obrigatório.'
                }
            }
        },
        id_linguagem: {
            row: '.col-sm-6',
            validators: {
                notEmpty: {
                    message: 'Informar a Linguagem é obrigatório.'
                }
            }
        },
        id_tipo_sistema: {
            row: '.col-sm-6',
            validators: {
                notEmpty: {
                    message: 'Informar o tipo de Sistema é obrigatório.'
                }
            }
        }
    }
});