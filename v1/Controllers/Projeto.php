<?php

namespace Controllers;
use Core\Controller;
use DataAccess\ProjetoDAO;
use DataAccess\ProjetoDTO;
use Helpers\Input;
use Slim\Slim;

class Projeto extends Controller
{
    public function __construct(ProjetoDAO $dataAccess)
    {
        $this->dataAccess = $dataAccess;
    }

    public function quem()
    {
        return 'Eu sou um projeto!';
    }

    public function getProjetos()
    {
        return $this->dataAccess->fullList();
    }

    public function getLinguagens()
    {
        return $this->dataAccess->getLinguagens();
    }

    public function getTiposSistema()
    {
        return $this->dataAccess->getTiposSistema();
    }

    public function getProjeto($id)
    {
        // Remover chaves do array literal do PostgreSQL
        $projeto = $this->dataAccess->getProjeto((int)$id);
        $projeto['valor_hora'] = (float)$projeto['valor_hora'];

        return $projeto;
    }

    public function getMetrica($id)
    {
        return $this->dataAccess->getMetrica((int)$id);
    }

    public function getPontosFuncao($id)
    {
        return $this->dataAccess->getPontosFuncao((int)$id);
    }

    public function salvarProjeto(array $projeto)
    {
        $projetoDTO = $this->setProjeto($projeto);
        $operacao = (Input::get('id_projeto')? UPDATE : INSERT);
        return $this->dataAccess->persist($projetoDTO, $operacao);
    }

    public function removerProjeto($id)
    {
        /** @var ProjetoDTO $dto */
        $dto = Slim::getInstance()->container['ProjetoDTO'];
        $dto->setIdProjeto($id);
        return $this->dataAccess->delete($dto, DELETE);
    }

    private function setProjeto(array $request_body)
    {
        Input::set($request_body);
        /** @var ProjetoDTO $dto */
        $dto = Slim::getInstance()->container['ProjetoDTO'];
        $dto
            ->setIdProjeto(Input::get('id_projeto'))
            ->setNomeProjeto(Input::get('nome_projeto'))
            ->setModelo('{' . trim(Input::get('modelo'), ',') . '}')
            ->setNivelInfluencia((int)Input::get('nivel_influencia'))
            ->setIdLinguagem((int)Input::get('id_linguagem'))
            ->setIdTipoSistema((int)Input::get('id_tipo_sistema'))
            ->setValorHora(getMoeda(Input::get('valor_hora')));

        return $dto;
    }

    public function findProjeto($id)
    {
        return $this->dataAccess->getById($id);
    }
}