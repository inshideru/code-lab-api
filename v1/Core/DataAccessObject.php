<?php

namespace Core;

abstract class DataAccessObject
{
    protected $dmlFunction;

    protected $success = false;
    protected $resultado;
    protected $numRegistros;

    /** @var \PDOStatement */
    protected $statement;

    protected $tabela; // tabela referente ao model
    protected $primaryKey; // chave primária da tabela
    /** @var  \PDO */
    protected $con;

    public function __construct()
    {
        $this->con = Database::getInstance();
    }

    /**
     * @return mixed
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    public function setTabela($tabela)
    {
        $this->tabela = $tabela;
        return $this;
    }

    /**
     * @param DataTransferObject $dto
     * @param $operacao
     * @return bool|mixed
     */
    public function persist(DataTransferObject $dto, $operacao)
    {
        $reflex = $dto->getReflex();

        $campos = ':' . implode(', :', array_keys($reflex)) . ', :operacao';

        $sql = "SELECT * FROM {$this->dmlFunction}({$campos})";

        foreach ($reflex as $atributo => $method) {
            $dados[$atributo] = $dto->{$method}();
        }

        $dados['operacao'] = $operacao;
        if ($this->query($sql, $dados)->success()) {
            return $this->first();
        }
        return false;
    }

    /**
     * @param DataTransferObject $dto
     * @return bool | DataTransferObject
     */
    protected function insert(DataTransferObject $dto)
    {
        $reflex = $dto->getReflex();
        unset($reflex[$this->primaryKey]);

        $colunas = implode(', ', array_keys($reflex));
        $campos = ':' . implode(', :', array_keys($reflex));

        $sql = "INSERT INTO {$this->tabela} ({$colunas})
                VALUES ({$campos}) returning *";

        foreach ($dto->getReflex() as $atributo => $method) {
            if ($atributo != $this->primaryKey) {
                $dados[$atributo] = $dto->{$method}();
            }
        }

        if ($this->query($sql, $dados)->success()) {
            return $this->getResultado()[0];
        }
        return false;
    }

    /**
     * @param DataTransferObject $dto
     * @return bool| DataTransferObject
     */
    protected function update(DataTransferObject $dto)
    {
        $parametros = array();
        foreach ($dto->getReflex() as $atributo => $method) {
            if ($atributo != 'cd_usuario_criacao'
                && $atributo != 'dt_usuario_criacao'
            ) {
                $parametros[] = $atributo . ' = :' . $atributo;
            }
        }
        $parametros = implode(', ', $parametros);

        $sql = "UPDATE {$this->tabela}
                SET {$parametros}
                WHERE {$this->primaryKey} = :{$this->primaryKey} returning *";

        $dados = array();
        foreach ($dto->getReflex() as $atributo => $method) {
            if ($atributo != 'cd_usuario_criacao' && $atributo != 'dt_usuario_criacao') {
                $dados[$atributo] = $dto->{$method}();
            }
        }

        if ($this->query($sql, $dados)->success()) {
            return $this->getResultado()[0];
        }
        return false;
    }

    /**
     * @param DataTransferObject $dto
     * @return bool | DataTransferObject
     */
    public function delete(DataTransferObject $dto)
    {
        $sql = "DELETE FROM {$this->tabela}
                WHERE {$this->primaryKey} = {$dto->{$dto->getReflex()[$this->primaryKey]}()}  returning *";

        if ($this->query($sql)->success()) {
            return $this->first();
        }
        return false;
    }

    /**
     * @param null $where
     * @param null $limit
     * @param null $offset
     * @param null $orderby
     * @return bool|mixed
     */
    public function select($where = null, $limit = null, $offset = null, $orderby = null)
    {
        $where = ($where != null ? " WHERE {$where}" : "");
        $limit = ($limit != null ? " LIMIT {$limit}" : "");
        $offset = ($offset != null ? " OFFSET {$offset}" : "");
        $orderby = ($orderby != null ? " ORDER BY {$orderby}" : "");

        $sql = "SELECT *
                FROM {$this->dbView}{$where}{$limit}{$offset}{$orderby}";

        if ($this->query($sql)->success()) {
            return $this->getResultado();
        }
        return false;
    }

    /**
     * @param $where
     * @return bool|mixed
     */
    public function get($where)
    {
        return $this->select($where, null, null, null);
    }

    /**
     * @param $id
     * @return bool
     */
    public function getById($id)
    {
        $this->resultado = $this->get("{$this->primaryKey} = {$id}");

        if ($this->resultado) {
            return $this->first();
        }

        return false;
    }

    /**
     * @param string $sql = SQL para ser preparada
     * @param array $parametros = dados a serem enviados para o banco de dados
     * @return $this
     */
    protected function query($sql, $parametros = null)
    {
        try {
            $this->statement = $this->con->prepare($sql);
            if (is_array($parametros) && count($parametros)) {
                $this->statement->execute($parametros);
            } else {
                $this->statement->execute();
            }

            $this->resultado = $this->statement->fetchAll(\PDO::FETCH_ASSOC);
            $this->numRegistros = $this->statement->rowCount();
            $this->success = true;
        } catch (\PDOException $e) {
            $this->success = false;
            CodeFail((int)$e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine());
        }

        return $this;
    }

    /**
     * @return bool
     */
    protected function success()
    {
        return $this->success;
    }

    /**
     * @return mixed
     */
    public function getResultado()
    {
        return $this->resultado;
    }

    /**
     * @return bool|DataTransferObject
     */
    public function fullList()
    {
        return $this->select(null, null, null, "{$this->primaryKey} DESC");

    }

    /**
     * @return mixed
     */
    public function first()
    {
        return $this->resultado[0];
    }

    /**
     * @return int $numregistros = numero de registros retornados da query
     */
    public function getNumRegistros()
    {
        return $this->numRegistros;
    }

}