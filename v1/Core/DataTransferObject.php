<?php

namespace Core;

abstract class DataTransferObject
{

    /** @var  array */
    protected $reflex;

    /**
     * Deve retornar um array associativo onde os índices são as colunas da tabela
     * e os valores são os métodos 'Getter' da respectiva coluna
     * @return array
     */
    public function getReflex()
    {
        return $this->reflex;
    }
    /**
     * @param $reflex
     * @return $this
     */
    public function setReflex($reflex)
    {
        $this->reflex = $reflex;
        return $this;
    }

} 