<?php

namespace Core;

use Slim\Slim;

abstract class Database {
    /** @var \PDO */
    private static $pdo;
    /** @var \PDO */
    private static $instance;

    private function __construct(){}

    private static function connect(\PDO $pdo)
    {
        try {
            self::$pdo = $pdo;
            self::$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return self::$pdo;
        } catch (\PDOException $e) {
            CodeFail((int)$e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine());
            die;
        }
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = Database::connect(Slim::getInstance()->container['PDO']);
        }
        return self::$instance;
    }

} 