## Servidor PostgreSQL
----
### Configurar timezone correto;
* Utilizando psql (terminal)
* set timezone = 'America/Sao_Paulo';
* Para exibir a configuração
* show timezone

### Configurar estilo da Data
* Utilizando psql (terminal)
* set datestyle = iso, dmy;
* Para exibir a configuração
* show datestyle