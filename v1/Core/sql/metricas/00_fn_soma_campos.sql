-- '-a' para mostrar os comandos executados em um arquivo
-- psql -h localhost -p 5432 -d pg_lab -a -U postgres -W
-- \i fn_metrica.sql;

CREATE OR REPLACE FUNCTION fn_soma_campos(p_array integer[], OUT p_soma integer)
    RETURNS integer AS
$$
DECLARE 
    i integer;
BEGIN
    p_soma := 0;
    FOREACH i IN ARRAY p_array
    LOOP
        p_soma := p_soma + i;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

