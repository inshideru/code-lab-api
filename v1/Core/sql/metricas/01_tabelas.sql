/*
CREATE DATABASE codelab
TEMPLATE TEMPLATE0
ENCODING 'utf-8'
CONNECTION LIMIT -1;
*/

DROP TABLE IF EXISTS tb_tipo_sistema CASCADE;
CREATE TABLE tb_tipo_sistema (
  id_tipo_sistema SERIAL,
  descricao       VARCHAR(60) CONSTRAINT nn_descricao_tb_tipo_sistema NOT NULL,
  produtividade   NUMERIC  CONSTRAINT nn_produtividade_tb_tipo_sistema NOT NULL,
  ---------------------------------------------------------------------------
  CONSTRAINT pk_id_tipo_sistema_tb_tipo_sistema PRIMARY KEY(id_tipo_sistema)
);

DROP TABLE IF EXISTS tb_linguagem CASCADE;
CREATE TABLE tb_linguagem (
  id_linguagem   SERIAL,
  nome_linguagem VARCHAR(30) CONSTRAINT nn_nome_linguagem_tb_linguagem NOT NULL,
  loc_por_fp     INTEGER CONSTRAINT nn_loc_por_fp_tb_linguagem NOT NULL,
  ---------------------------------------------------------------------------
  CONSTRAINT pk_id_linguagem_tb_linguagem PRIMARY KEY(id_linguagem)
);

DROP TABLE IF EXISTS tb_projeto CASCADE;
CREATE TABLE tb_projeto (
  id_projeto       SERIAL,
  nome_projeto     VARCHAR(60) CONSTRAINT nn_nome_projeto_tb_projeto NOT NULL,
  modelo           INTEGER[] CONSTRAINT nn_modelo_tb_projeto NOT NULL,
  nivel_influencia INTEGER CONSTRAINT nn_nivel_influencia_tb_projeto NOT NULL,
  id_linguagem     INTEGER CONSTRAINT nn_id_linguagem_tb_projeto NOT NULL,
  id_tipo_sistema  INTEGER CONSTRAINT nn_id_tipo_sistema_tb_projeto NOT NULL,
  valor_hora       NUMERIC CONSTRAINT nn_valor_hora_tb_projeto NOT NULL,
  ---------------------------------------------------------------------------
  CONSTRAINT pk_id_projeto_tb_projeto PRIMARY KEY(id_projeto),
  CONSTRAINT fk_id_linguagem_tb_projeto FOREIGN KEY (id_linguagem)
    REFERENCES tb_linguagem(id_linguagem),
  CONSTRAINT fk_id_tipo_sistema_tb_projeto FOREIGN KEY (id_tipo_sistema)
    REFERENCES tb_tipo_sistema(id_tipo_sistema)
);

DROP TABLE IF EXISTS tb_funcao CASCADE;
CREATE TABLE tb_funcao (
  id_funcao   SERIAL,
  nome_funcao VARCHAR(30) CONSTRAINT nn_nome_funcao_tb_funcao NOT NULL,
  ---------------------------------------------------------------------------
  CONSTRAINT pk_id_funcao_tb_funcao PRIMARY KEY(id_funcao)
);

DROP TABLE IF EXISTS tb_complexidade CASCADE;
CREATE TABLE tb_complexidade (
  id_complexidade   SERIAL,
  nome_complexidade VARCHAR(30) CONSTRAINT nn_nome_complexidade_tb_complexidade NOT NULL,
  ---------------------------------------------------------------------------
  CONSTRAINT pk_id_complexidade_tb_complexidade PRIMARY KEY(id_complexidade)
);

DROP TABLE IF EXISTS tb_funcao_complexidade CASCADE;
CREATE TABLE tb_funcao_complexidade (
  id_funcao INTEGER,
  id_complexidade INTEGER,
  peso INTEGER CONSTRAINT nn_peso_tb_funcao_complexidade NOT NULL,
  ---------------------------------------------------------------------------
  CONSTRAINT pk_id_funcao_id_complexidade_tb_funcao_complexidade PRIMARY KEY(id_funcao, id_complexidade),
  CONSTRAINT fk_id_funcao_tb_funcao_complexidade FOREIGN KEY (id_funcao)
    REFERENCES tb_funcao(id_funcao),
  CONSTRAINT fk_id_complexidade_tb_funcao_complexidade FOREIGN KEY (id_complexidade)
    REFERENCES tb_complexidade(id_complexidade)
);

DROP TABLE IF EXISTS tb_projeto_ponto_funcao CASCADE;
CREATE TABLE tb_projeto_ponto_funcao (
  id_projeto      INTEGER,
  id_funcao       INTEGER,
  id_complexidade INTEGER,
  quantidade      INTEGER CONSTRAINT nn_quantidade_tb_projeto_ponto_funcao NOT NULL,
  ---------------------------------------------------------------------------
  CONSTRAINT pk_id_projeto_id_funcao_tb_projeto_ponto_funcao PRIMARY KEY(id_projeto, id_funcao, id_complexidade),
  CONSTRAINT fk_id_projeto_tb_projeto_ponto_funcao FOREIGN KEY (id_projeto)
    REFERENCES tb_projeto(id_projeto),
  CONSTRAINT fk_id_funcao_tb_projeto_ponto_funcao FOREIGN KEY (id_funcao)
    REFERENCES tb_funcao(id_funcao),
  CONSTRAINT fk_id_complexidade_tb_projeto_ponto_funcao FOREIGN KEY (id_complexidade)
    REFERENCES tb_complexidade(id_complexidade)
);

DROP TABLE IF EXISTS tb_metrica CASCADE;
CREATE TABLE tb_metrica (
  id_metrica             INTEGER,
  m_custo                NUMERIC CONSTRAINT nn_custo_tb_metrica NOT NULL,
  m_tempo                VARCHAR(30) CONSTRAINT nn_tempo_tb_metrica NOT NULL,
  m_fp_bruto             INTEGER CONSTRAINT nn_fp_bruto_tb_metrica NOT NULL,
  m_fator_ajuste         NUMERIC CONSTRAINT nn_fator_ajuste_tb_metrica NOT NULL,
  m_tempo_bruto          VARCHAR(60) CONSTRAINT nn_tempo_bruto_tb_metrica NOT NULL,
  m_custo_linguagem      NUMERIC CONSTRAINT nn_custo_linguagem_tb_metrica NOT NULL,
  m_mes_perc             NUMERIC CONSTRAINT nn_mer_perc_tb_metrica NOT NULL,
  m_horas_totais_projeto NUMERIC CONSTRAINT nn_horas_totais_projeto_tb_metrica NOT NULL,
  ---------------------------------------------------------------------------
  CONSTRAINT pk_id_projeto_tb_metrica PRIMARY KEY (id_metrica),
  CONSTRAINT fk_id_projeto_tb_metrica FOREIGN KEY (id_metrica)
    REFERENCES tb_projeto(id_projeto)
);
