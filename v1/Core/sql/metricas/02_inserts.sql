INSERT INTO tb_funcao
(nome_funcao)
VALUES
('Entrada'),
('Saída'),
('Consulta'),
('Arquivos'),
('Interfaces');

INSERT INTO tb_complexidade
(nome_complexidade)
VALUES
('Simples'),
('Medio'),
('Complexo');

INSERT INTO tb_funcao_complexidade
(id_funcao, id_complexidade, peso)
VALUES
(1, 1, 3),
(1, 2, 4),
(1, 3, 6),
(2, 1, 4),
(2, 2, 5),
(2, 3, 7),
(3, 1, 3),
(3, 2, 4),
(3, 3, 6),
(4, 1, 7),
(4, 2, 10),
(4, 3, 15),
(5, 1, 5),
(5, 2, 7),
(5, 3, 10);

INSERT INTO tb_linguagem
(nome_linguagem, loc_por_fp)
VALUES
('Cobol', 100), -- 1
('Pascal', 90), -- 2
('C++', 30), -- 3
('Java', 20), -- 4
('Delphi', 20), -- 5
('Visual Basic', 20), -- 6
('SQL', 15),  -- 7
('HTML', 15);  -- 8

INSERT INTO tb_tipo_sistema
(descricao, produtividade)
VALUES
('Sistema Comercial',2500), -- 1
('Comércio Eletrônico', 3600), -- 2
('Sistema Web', 3300); -- 3