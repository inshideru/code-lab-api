CREATE OR REPLACE FUNCTION fn_tg_projeto_ponto_funcao() RETURNS TRIGGER AS
$$
DECLARE
  v_entrada RECORD;
  v_saida RECORD;
  v_consulta RECORD;
  v_arquivo RECORD;
  v_interface RECORD;

BEGIN
  SELECT * INTO v_entrada FROM fn_entrada(new.modelo);
  SELECT * INTO v_saida FROM fn_saida(new.modelo);
  SELECT * INTO v_consulta FROM fn_consulta(new.modelo);
  SELECT * INTO v_arquivo FROM fn_arquivo(new.modelo);
  SELECT * INTO v_interface FROM fn_interface(new.modelo);
  
  IF TG_OP = 'INSERT' THEN
    
    INSERT INTO tb_projeto_ponto_funcao
    (id_projeto, id_funcao, id_complexidade, quantidade)
    VALUES
    -- Entrada
    (new.id_projeto, 1, 1, v_entrada.simples),
    (new.id_projeto, 1, 2, v_entrada.medio),
    (new.id_projeto, 1, 3, v_entrada.complexo),
    -- Saída
    (new.id_projeto, 2, 1, v_saida.simples),
    (new.id_projeto, 2, 2, v_saida.medio),
    (new.id_projeto, 2, 3, v_saida.complexo),
    -- Consulta
    (new.id_projeto, 3, 1, v_consulta.simples),
    (new.id_projeto, 3, 2, v_consulta.medio),
    (new.id_projeto, 3, 3, v_consulta.complexo),
    -- Arquivo
    (new.id_projeto, 4, 1, v_arquivo.simples),
    (new.id_projeto, 4, 2, v_arquivo.medio),
    (new.id_projeto, 4, 3, v_arquivo.complexo),
    -- Interface
    (new.id_projeto, 5, 1, v_interface.simples),
    (new.id_projeto, 5, 2, v_interface.medio),
    (new.id_projeto, 5, 3, v_interface.complexo);

  ELSIF TG_OP = 'UPDATE' THEN
      -- Entrada
    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_entrada.simples
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 1
      AND id_complexidade = 1;

    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_entrada.medio
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 1
      AND id_complexidade = 2;

    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_entrada.complexo
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 1
      AND id_complexidade = 3;

    -- Saída
    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_saida.simples
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 2
      AND id_complexidade = 1;

    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_saida.medio
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 2
      AND id_complexidade = 2;

    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_saida.complexo
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 2
      AND id_complexidade = 3;

    -- Consulta
    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_consulta.simples
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 3
      AND id_complexidade = 1;

    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_consulta.medio
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 3
      AND id_complexidade = 2;

    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_consulta.complexo
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 3
      AND id_complexidade = 3;

    -- Arquivo
    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_arquivo.simples
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 4
      AND id_complexidade = 1;

    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_arquivo.medio
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 4
      AND id_complexidade = 2;

    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_arquivo.complexo
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 4
      AND id_complexidade = 3;

    -- Interface
    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_interface.simples
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 5
      AND id_complexidade = 1;

    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_interface.medio
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 5
      AND id_complexidade = 2;

    UPDATE tb_projeto_ponto_funcao SET
      quantidade = v_interface.complexo
    WHERE id_projeto = new.id_projeto
      AND id_funcao = 5
      AND id_complexidade = 3;

    END IF;

  RETURN NULL;
END
$$ LANGUAGE plpgsql;

/*
DROP TRIGGER IF EXISTS tg_projeto_ponto_funcao ON tb_projeto;
*/
CREATE TRIGGER tg_projeto_ponto_funcao
AFTER INSERT OR UPDATE ON tb_projeto
FOR EACH ROW EXECUTE PROCEDURE fn_tg_projeto_ponto_funcao();


CREATE OR REPLACE FUNCTION fn_tg_projeto_delete() RETURNS TRIGGER AS
$$
BEGIN

  DELETE FROM tb_projeto_ponto_funcao
  WHERE id_projeto = old.id_projeto;

  RETURN old;
END
$$ LANGUAGE plpgsql;

/*
DROP TRIGGER IF EXISTS tg_projeto_delete ON tb_projeto;
*/
CREATE TRIGGER tg_projeto_delete
BEFORE DELETE ON tb_projeto
FOR EACH ROW EXECUTE PROCEDURE fn_tg_projeto_delete();
