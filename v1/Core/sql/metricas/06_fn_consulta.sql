CREATE OR REPLACE FUNCTION fn_consulta(p_modelo integer[])
RETURNS TABLE (simples integer, medio integer, complexo integer) AS
$$
DECLARE
  i INTEGER;

  v_simples INTEGER := 0;
  v_medio INTEGER := 0;
  v_complexo INTEGER := 0;

  v_colunas INTEGER[];
BEGIN
  v_colunas[1] := 0;
  v_colunas[2] := 0;
  v_colunas[3] := 0;
  
  p_modelo := p_modelo || fn_soma_campos(p_modelo);

  -- Conta colunas
  FOR i IN 1..array_length(p_modelo, 1)
  LOOP
    IF p_modelo[i] BETWEEN 1 AND 4 THEN -- coluna 1
      v_colunas[1] := v_colunas[1] + 1;

    ELSIF p_modelo[i] BETWEEN 5 AND 15 THEN -- coluna 2
      v_colunas[2] := v_colunas[2] + 1;

    ELSIF p_modelo[i] > 15 THEN -- coluna 3
      v_colunas[3] := v_colunas[3] + 1;

    ELSE
      RAISE NOTICE 'i = %',i;
    END IF;
    --RAISE NOTICE 'i = %',p_modelo[i] ;
  END LOOP;

  -- Coluna 1
  IF v_colunas[1] < 2 THEN
    v_simples := v_simples + v_colunas[1];

  ELSIF v_colunas[1] = 2 THEN
    v_simples := v_simples + v_colunas[1];

  ELSIF v_colunas[1] > 2 THEN
    v_medio := v_medio + v_colunas[1];
  END IF;

  -- Coluna 2
  IF v_colunas[2] < 2 THEN
    v_simples := v_simples + v_colunas[2];

  ELSIF v_colunas[2] = 2 THEN
    v_medio := v_medio + v_colunas[2];

  ELSIF v_colunas[2] > 2 THEN
    v_complexo := v_complexo + v_colunas[2];
  END IF;

  -- Coluna 3
  IF v_colunas[3] < 2 THEN
    v_medio := v_medio + v_colunas[3];

  ELSIF v_colunas[3] = 2 THEN
    v_complexo := v_complexo + v_colunas[3];

  ELSIF v_colunas[3] > 2 THEN
    v_complexo := v_complexo + v_colunas[3];
  END IF;

  RETURN QUERY SELECT v_simples,v_medio,v_complexo;
  
END
$$ LANGUAGE plpgsql;