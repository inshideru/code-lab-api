CREATE OR REPLACE FUNCTION fn_fator_ajuste(p_nivel_influencia integer)
RETURNS NUMERIC AS
$$
BEGIN
  RETURN 0.65 + (0.01 * p_nivel_influencia);
END
$$ LANGUAGE plpgsql;