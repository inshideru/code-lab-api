CREATE OR REPLACE FUNCTION fn_fp_bruto(p_modelo integer[])
RETURNS INTEGER AS
$$
BEGIN
  RETURN fn_entrada(p_modelo) + fn_saida(p_modelo) + fn_consulta(p_modelo) + fn_arquivo(p_modelo) + fn_interface(p_modelo);
END
$$ LANGUAGE plpgsql;