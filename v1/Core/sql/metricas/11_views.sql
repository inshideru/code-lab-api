CREATE VIEW vw_funcao_complexidade AS
SELECT
  id_funcao, id_complexidade, nome_funcao, nome_complexidade, peso
FROM tb_funcao_complexidade
NATURAL JOIN tb_funcao
NATURAL JOIN tb_complexidade;

CREATE VIEW vw_projeto AS
SELECT
  id_projeto, 
  nome_projeto,
  modelo,
  nivel_influencia,
  id_linguagem,
  nome_linguagem,
  loc_por_fp AS linguagem_linhas_por_funcao,
  id_tipo_sistema,
  descricao AS tipo_sistema,
  produtividade,
  valor_hora
FROM tb_projeto
NATURAL JOIN tb_linguagem
NATURAL JOIN tb_tipo_sistema
ORDER BY id_projeto;

CREATE VIEW vw_projeto_ponto_funcao AS
SELECT
  id_projeto,
  nome_projeto,
  nome_funcao AS funcao,
  quantidade AS nr_ocorrencias,
  nome_complexidade,
  peso,
  (quantidade * peso) AS resultado,
  sum(quantidade * peso) OVER (PARTITION BY id_projeto) AS fp_bruto
FROM tb_projeto_ponto_funcao
NATURAL JOIN tb_projeto
NATURAL JOIN tb_funcao
NATURAL JOIN tb_complexidade
NATURAL JOIN tb_funcao_complexidade
ORDER BY id_projeto, id_funcao, id_complexidade;

CREATE VIEW vw_projeto_fp_bruto AS
SELECT distinct id_projeto, nome_projeto, fp_bruto
FROM vw_projeto_ponto_funcao
ORDER BY id_projeto;
