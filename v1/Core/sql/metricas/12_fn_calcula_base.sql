CREATE OR REPLACE FUNCTION fn_calcula_base(
	p_id_projeto tb_projeto.id_projeto%TYPE
	)
RETURNS INTEGER AS
$$
DECLARE 
  v_registro tb_projeto%ROWTYPE;
  v_calculo INTEGER;
BEGIN
  --SELECT fn_fp_bruto(p_modelo) * fn_fator_ajuste(p_nivel_influencia) INTO v_calculo;
  SELECT * INTO v_registro
  FROM tb_projeto
  WHERE id_projeto = p_id_projeto;

  IF NOT FOUND THEN
    RAISE EXCEPTION 'Não foi encontrado projeto com o id %', p_id_projeto;
  END IF;

  SELECT 
    round(fp_bruto * fn_fator_ajuste(v_registro.nivel_influencia))::INTEGER 
  INTO v_calculo
  FROM vw_projeto_fp_bruto
  WHERE id_projeto = p_id_projeto;

  RETURN v_calculo;
END
$$ LANGUAGE plpgsql;