  CREATE OR REPLACE FUNCTION fn_metrica(
    p_id_projeto tb_projeto.id_projeto%TYPE
  ) RETURNS TABLE(
                id_metrica             INTEGER,
                m_custo                NUMERIC ,
                m_tempo                VARCHAR(30),
                m_fp_bruto             INTEGER,
                m_fator_ajuste         NUMERIC,
                m_tempo_bruto          VARCHAR(60),
                m_custo_linguagem      NUMERIC,
                m_mes_perc             NUMERIC,
                m_horas_totais_projeto NUMERIC) AS
  $$
  DECLARE
    v_registro vw_projeto%ROWTYPE;

    v_custo_linguagem NUMERIC;
    v_tempo NUMERIC;
    v_hora_mes NUMERIC;
    v_msg TEXT;

    v_meses NUMERIC;
    v_dias NUMERIC;
    v_horas VARCHAR;
    v_fp_bruto INTEGER;
    v_fator_ajuste NUMERIC;
  BEGIN
    SELECT *  INTO v_registro
    FROM vw_projeto
    WHERE id_projeto = p_id_projeto;

    IF NOT FOUND THEN
      RAISE EXCEPTION 'Não existe projeto com o id %', p_id_projeto;
    END IF;

    v_custo_linguagem := fn_calcula_base(v_registro.id_projeto) * v_registro.linguagem_linhas_por_funcao;
    -- Trunca (só pega 2 números após a vírgula)
    v_tempo := trunc(v_custo_linguagem / v_registro.produtividade,2); -- Porcentagem
    -- Total de horas
    v_hora_mes := v_tempo * 132; -- 132 -> horas no mes
    
    v_meses := trunc(v_tempo);
    v_dias := trunc((v_tempo - v_meses) * 22); -- 22 dias no mes
    v_horas := ((v_tempo - v_meses) * 22 - v_dias) * 6;
    
    RAISE NOTICE '======================= METRICA ====================================';
    RAISE NOTICE '% Meses, % Dias, % Horas',v_meses, (v_tempo - v_meses) * 22, ((v_tempo - v_meses) * 22 - v_dias) * 6;

    v_horas := justify_hours((v_horas||' hours')::interval)::TEXT;

    RAISE NOTICE 'Custo da linguagem %', v_custo_linguagem;
    RAISE NOTICE 'Kloc sistema %', v_registro.produtividade;

    RAISE NOTICE 'Custo de tempo % (resultado da divisão em meses)',v_tempo;
    RAISE NOTICE 'Custo de tempo % x 132 Horas/Mês = % Horas',v_tempo, v_hora_mes;

    v_msg := 'Custo total R$ '||v_hora_mes * v_registro.valor_hora||' Tempo: '||v_meses||'m '||v_dias||'d '||v_horas;

    SELECT fp_bruto INTO v_fp_bruto
    FROM  vw_projeto_fp_bruto
    WHERE id_projeto = v_registro.id_projeto;

    v_fator_ajuste := fn_fator_ajuste(v_registro.nivel_influencia);

    RETURN QUERY SELECT 
                  v_registro.id_projeto,
                  v_hora_mes * v_registro.valor_hora,
                  (v_meses||'m '||v_dias||'d '||v_horas)::VARCHAR,
                  v_fp_bruto,
                  v_fator_ajuste,
                  (v_meses||' Meses, '||(v_tempo - v_meses) * 22||' Dias, '||((v_tempo - v_meses) * 22 - v_dias) * 6||' Horas')::VARCHAR,
                  v_custo_linguagem,
                  v_tempo,
                  v_hora_mes;
  END
  $$ LANGUAGE plpgsql;

--DROP VIEW vw_metrica;
CREATE VIEW vw_metrica AS
  SELECT
    p.id_projeto,
    p.nome_projeto,
    p.modelo,
    p.nivel_influencia,
    p.id_linguagem,
    p.nome_linguagem,
    p.linguagem_linhas_por_funcao,
    p.id_tipo_sistema,
    p.tipo_sistema,
    m.m_horas_totais_projeto AS horas_totais_projeto,
    to_char(p.valor_hora,'"R$ " 999G999G990D99') AS valor_hora,
    to_char(m.m_custo,'"R$ " 999G999G990D99') AS custo_projeto,
    m.m_tempo AS tempo_necessario,
    m.m_fp_bruto AS fp_bruto,
    m.m_fator_ajuste AS fator_ajuste,
    ROUND(m.m_fp_bruto * m.m_fator_ajuste) AS base,
    m.m_tempo_bruto AS tempo_bruto,
    m.m_custo_linguagem AS custo_linguagem,
    p.produtividade,
    m.m_mes_perc AS mes_percentual
  FROM vw_projeto p, tb_metrica m
  WHERE p.id_projeto = m.id_metrica;
