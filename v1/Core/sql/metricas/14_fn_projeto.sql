CREATE OR REPLACE FUNCTION fn_projeto(
  p_id_projeto tb_projeto.id_projeto%TYPE,
  p_nome_projeto tb_projeto.nome_projeto%TYPE,
  p_modelo tb_projeto.modelo%TYPE,
  p_nivel_influencia tb_projeto.nivel_influencia%TYPE,
  p_id_linguagem tb_projeto.id_linguagem%TYPE,
  p_id_tipo_sistema tb_projeto.id_tipo_sistema%TYPE,
  p_valor_hora tb_projeto.valor_hora%TYPE,
  operacao char(1)
) RETURNS vw_metrica AS
  $$
  DECLARE
    v_controle INTEGER;
    v_registro vw_metrica%ROWTYPE;
    v_metrica RECORD;
    msg_exception TEXT;
  BEGIN
    SELECT id_projeto INTO v_controle
    FROM tb_projeto
    WHERE id_projeto = p_id_projeto;

    IF operacao = 'I' THEN
      IF v_controle IS NULL THEN
        INSERT INTO tb_projeto
        (nome_projeto, modelo, nivel_influencia, id_linguagem, id_tipo_sistema, valor_hora)
        VALUES
          (p_nome_projeto, p_modelo, p_nivel_influencia, p_id_linguagem, p_id_tipo_sistema, p_valor_hora)
        RETURNING id_projeto INTO v_controle;

        SELECT * INTO v_metrica FROM (SELECT * FROM fn_metrica(v_controle)) mt;

        INSERT INTO tb_metrica
        (id_metrica, m_custo, m_tempo, m_fp_bruto, m_fator_ajuste, m_tempo_bruto, m_custo_linguagem, m_mes_perc, m_horas_totais_projeto)
        VALUES
          (v_controle, v_metrica.m_custo, v_metrica.m_tempo, v_metrica.m_fp_bruto, v_metrica.m_fator_ajuste, v_metrica.m_tempo_bruto, v_metrica.m_custo_linguagem, v_metrica.m_mes_perc, v_metrica.m_horas_totais_projeto)
        ;

        SELECT * INTO v_registro
        FROM vw_metrica
        WHERE id_projeto = v_controle;

      ELSE
        RAISE EXCEPTION 'Não informe um id de projeto para instrução INSERT.';
      END IF;

    ELSIF operacao = 'U' THEN
      IF v_controle IS NOT NULL THEN
        UPDATE tb_projeto SET
          nome_projeto = p_nome_projeto,
          modelo = p_modelo,
          nivel_influencia = p_nivel_influencia,
          id_linguagem = p_id_linguagem,
          id_tipo_sistema = p_id_tipo_sistema,
          valor_hora = p_valor_hora
        WHERE id_projeto = p_id_projeto
        RETURNING id_projeto INTO v_controle;

        SELECT * INTO v_metrica FROM fn_metrica(v_controle);
        UPDATE tb_metrica SET
          m_custo = v_metrica.m_custo,
          m_tempo = v_metrica.m_tempo,
          m_fp_bruto = v_metrica.m_fp_bruto,
          m_fator_ajuste = v_metrica.m_fator_ajuste,
          m_tempo_bruto = v_metrica.m_tempo_bruto,
          m_custo_linguagem = v_metrica.m_custo_linguagem,
          m_mes_perc = v_metrica.m_mes_perc,
          m_horas_totais_projeto = v_metrica.m_horas_totais_projeto
        WHERE id_metrica = v_controle;

        SELECT * INTO v_registro
        FROM vw_metrica
        WHERE id_projeto = v_controle;

      ELSE
        RAISE EXCEPTION 'Projeto não encontrado.';
      END IF;

    ELSIF operacao = 'D' THEN
      IF v_controle IS NOT NULL THEN
        SELECT * INTO v_registro
        FROM vw_metrica
        WHERE id_projeto = v_controle;

        DELETE FROM tb_metrica
        WHERE id_metrica = v_controle;

        DELETE FROM  tb_projeto
        WHERE id_projeto = p_id_projeto;

      ELSE
        RAISE EXCEPTION 'Projeto não encontrado.';
      END IF;
    ELSE
      RAISE EXCEPTION 'Operação inválida.';
    END IF;

    RETURN v_registro;

    EXCEPTION
    WHEN foreign_key_violation THEN
      GET STACKED DIAGNOSTICS msg_exception = RETURNED_SQLSTATE;
      RAISE EXCEPTION 'Esta operação viola uma restrição. Código %',msg_exception;

    WHEN invalid_text_representation THEN
      GET STACKED DIAGNOSTICS msg_exception = RETURNED_SQLSTATE;
      RAISE EXCEPTION 'Valor inválido para o campo. Código %',msg_exception;

    WHEN not_null_violation THEN
      GET STACKED DIAGNOSTICS msg_exception = RETURNED_SQLSTATE;
      RAISE EXCEPTION 'O campo não pode conter valor nulo. Código %',msg_exception;

    WHEN others THEN
      GET STACKED DIAGNOSTICS msg_exception = MESSAGE_TEXT;
      RAISE NOTICE 'Erro: %',msg_exception;
      RAISE EXCEPTION 'Algo terrível aconteceu!';
  END
  $$ LANGUAGE plpgsql;