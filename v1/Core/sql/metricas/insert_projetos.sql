SELECT fn_projeto
(
  NULL, -- Id do Projeto
  'Projeto da Apostila', -- Nome do Projeto
  '{6,7,4,2,2,5,2,5,6}'::integer[], -- Modelo
  53, -- Nível de Influência
  4, -- Java
  3, -- Sistema Web
  90, -- Valor Hora
  'I' -- Operação
);
SELECT fn_projeto
(
  NULL, -- Id do Projeto
  'Projeto da 1ª Aula de Métricas', -- Nome do Projeto
  '{7,9,15,21}'::integer[], -- Modelo
  70, -- Nível de Influência
  4, -- Java
  3, -- Sistema Web
  30, -- Valor Hora
  'I' -- Operação
);
SELECT fn_projeto
(
  NULL, -- Id do Projeto
  'Projeto Alpha', -- Nome do Projeto
  '{9,12,4,10}'::integer[], -- Modelo
  70, -- Nível de Influência
  4, -- Java
  1, -- Sistema Comercial
  25, -- Valor Hora
  'I' -- Operação
);
SELECT fn_projeto
(
  NULL, -- Id do Projeto
  'Projeto Betta', -- Nome do Projeto
  '{12,9,13,8,3,2,19,4}'::integer[], -- Modelo
  70, -- Nível de Influência
  6, -- Visual Basic
  3, -- Sistema web
  40, -- Valor Hora
  'I' -- Operação
);
SELECT fn_projeto
(
  NULL, -- Id do Projeto
  'dnaCRM', -- Nome do Projeto
  '{9,6,10,8,3,13,10,16,6,26,16,14,15,12,15,12,6,8,8,13,9}'::integer[], -- Modelo
  70, -- Nível de Influência
  6, -- Visual Basic
  3, -- Sistema web
  30, -- Valor Hora
  'I' -- Operação
);