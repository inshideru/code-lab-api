<?php

namespace DataAccess;

use Core\DataAccessObject;
use Core\DataTransferObject;

class ProjetoDAO extends DataAccessObject
{

    public function __construct()
    {
        parent::__construct();
        $this->dmlFunction = 'fn_projeto';
        $this->tabela = 'tb_projeto';
        $this->dbView = 'vw_metrica';
        $this->primaryKey = 'id_projeto';
    }

    /**
     * @param ProjetoDTO $dto
     * @return bool|mixed
     */
    public function gravar(ProjetoDTO $dto)
    {
        if ($dto->getIdProjeto()) {
            $obj = $this->persist($dto,UPDATE);
        } else {
            $obj = $this->persist($dto,INSERT);
        }
        return $obj;
    }

    public function delete(DataTransferObject $dto)
    {
        return $this->persist($dto,DELETE);
    }

    public function getProjeto($id)
    {
        $id = (int)$id;

        $sql ="SELECT * FROM tb_projeto
               WHERE id_projeto = {$id}";

        if ($this->query($sql)->success()) {
            return $this->getResultado()[0];
        }
        return false;
    }

    public function getMetrica($id)
    {
        $id = (int)$id;

        $sql ="SELECT * FROM vw_metrica
               WHERE id_projeto = {$id}";

        if ($this->query($sql)->success()) {
            return $this->getResultado()[0];
        }
        return false;
    }

    public function getPontosFuncao($id)
    {
        $id = (int)$id;

        return $this->query(
            "SELECT * FROM vw_projeto_ponto_funcao
             WHERE id_projeto = {$id}")->getResultado();
    }

    public function getLinguagens()
    {
        return $this->query(
            "SELECT * FROM tb_linguagem")->getResultado();
    }

    public function getTiposSistema()
    {
        return $this->query(
            "SELECT * FROM tb_tipo_sistema")->getResultado();
    }
}