<?php
/**
 * Created by PhpStorm.
 * User: Vinicius
 * Date: 06/06/2015
 * Time: 12:14
 */

namespace DataAccess;

use Core\DataTransferObject;

class ProjetoDTO extends DataTransferObject
{
    private $id_projeto;
    private $nome_projeto;
    private $modelo;
    private $nivel_influencia;
    private $id_linguagem;
    private $id_tipo_sistema;
    private $valor_hora;

    public function __construct()
    {
        $this->reflex = array(
            'id_projeto' => 'getIdProjeto',
            'nome_projeto' => 'getNomeProjeto',
            'modelo' => 'getModelo',
            'nivel_influencia' => 'getNivelInfluencia',
            'id_linguagem' => 'getIdLinguagem',
            'id_tipo_sistema' => 'getIdTipoSistema',
            'valor_hora' => 'getValorHora'
        );
    }

    /**
     * @return mixed
     */
    public function getIdProjeto()
    {
        return $this->id_projeto;
    }

    /**
     * @return mixed
     */
    public function getNomeProjeto()
    {
        return $this->nome_projeto;
    }

    /**
     * @return mixed
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * @return mixed
     */
    public function getNivelInfluencia()
    {
        return $this->nivel_influencia;
    }

    /**
     * @return mixed
     */
    public function getIdLinguagem()
    {
        return $this->id_linguagem;
    }

    /**
     * @return mixed
     */
    public function getIdTipoSistema()
    {
        return $this->id_tipo_sistema;
    }

    /**
     * @return mixed
     */
    public function getValorHora()
    {
        return $this->valor_hora;
    }

    /**
     * @param $id_projeto
     * @return $this
     */
    public function setIdProjeto($id_projeto)
    {
        $this->id_projeto = $id_projeto;
        return $this;
    }

    /**
     * @param $nome_projeto
     * @return $this
     */
    public function setNomeProjeto($nome_projeto)
    {
        $this->nome_projeto = $nome_projeto;
        return $this;
    }

    /**
     * @param $modelo
     * @return $this
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
        return $this;
    }

    /**
     * @param $nivel_influencia
     * @return $this
     */
    public function setNivelInfluencia($nivel_influencia)
    {
        $this->nivel_influencia = $nivel_influencia;
        return $this;
    }

    /**
     * @param $id_linguagem
     * @return $this
     */
    public function setIdLinguagem($id_linguagem)
    {
        $this->id_linguagem = $id_linguagem;
        return $this;
    }

    /**
     * @param $id_tipo_sistema
     * @return $this
     */
    public function setIdTipoSistema($id_tipo_sistema)
    {
        $this->id_tipo_sistema = $id_tipo_sistema;
        return $this;
    }

    /**
     * @param $valor_hora
     * @return $this
     */
    public function setValorHora($valor_hora)
    {
        $this->valor_hora = $valor_hora;
        return $this;
    }
}