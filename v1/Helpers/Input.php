<?php

namespace Helpers;

class Input
{
    private static $input;

    public static function set(array $input)
    {
        self::$input = $input;
    }

    /**
     * Retorna um valor armazenado em $_POST ou $_GET
     * @param $item
     * @return null|string
     */
    public static function get($item)
    {
        if (isset(self::$input[$item])) {
            return (trim(self::$input[$item]) == '' ? null : trim(self::$input[$item]));
        }
        return null;
    }

    /**
     * Recebe uma string e subtitui caracteres com pontuação
     * ou inválidos para nome de arquivo por traço '-'
     * @param string $nome = String a ser limpa
     * @return string
     */
    public static function limpar($nome)
    {
        $format = array();
        $format['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:,\\\'<>°ºª';
        $format['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';

        $string = strtr(utf8_decode($nome), utf8_decode($format['a']), $format['b']);
        $string = strip_tags(trim($string));
        $string = str_replace(' ', '-', $string);
        $string = str_replace(array('-----', '----', '---', '--'), '-', $string);

        return strtolower(utf8_encode($string));

    }

    /**
     * Recebe um string e retorna a mesma após remover os caracteres
     * '.', '-', '(', ')', ' '
     * Propósito: Receber um núemro de telefone ou CPF e remover os caracteres não numéricos
     * @param $string
     * @return mixed
     */
    public static function clean($string)
    {
        return str_replace(array('.', '-', '(', ')', ' '), '', $string);
    }

    /**
     * Recebe uma string ou array
     * Caso seja informado um array,
     * verifica se há alguma string vazia ('') e transforma em tipo de dado 'null'
     * Se $dados for somente uma string, verifica se a string é vazia e transforma e null
     * @param mixed $dados
     * @return mixed
     */
    public static function emptyToNull($dados)
    {
        if (is_array($dados)) {
            foreach ($dados as $campo => $conteudo) {
                $dados[$campo] = (!empty($conteudo) ? $conteudo : null);
            }
        } else {
            $dados = (!empty($dados) ? $dados : null);
        }

        return $dados;
    }
}
