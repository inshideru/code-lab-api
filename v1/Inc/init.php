<?php
define('DIA', $app->container['calendario']->format('d'));
define('MES', $app->container['meses'][(int)$app->container['calendario']->format('m')]);
define('ANO', $app->container['calendario']->format('Y'));

/** Constantes para uso em funções de DML da Classe DataAccessObject */
define('INSERT', 'I');
define('UPDATE', 'U');
define('DELETE', 'D');

function escape($string)
{
    return htmlentities($string, ENT_QUOTES, 'UTF-8');
}

function getMoeda($stringValor)
{
    if ($stringValor != null) {
        $original = array('.', ',');
        $final = array('', '.');
        $valor = str_replace($original, $final, $stringValor);

        return (double)$valor;
    }
    return null;
}

function CodeFail($numero, $mensagem, $arquivo, $linha){
    $response = array(
        'erro' => true,
        'numero' => $numero,
        'msg' => strchr($mensagem,'ERROR: ', false)
    );
    echoJSON(400, $response);
    \Slim\Slim::getInstance()->stop();
}

function echoJSON($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    $app->status($status_code);
    $app->contentType('application/json;charset=utf-8');

    echo json_encode($response, JSON_PRETTY_PRINT);
}

function checkContetType()
{
    $headers = apache_request_headers();

    if ($headers['Content-Type'] != 'application/json; charset=UTF-8') {
        $response = array(
            'erro' => true,
            'msg' => 'Os dados devem ser enviados no formato application/json'
        );
        echoJSON(415, $response);
        \Slim\Slim::getInstance()->stop();
    }

}

function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST; //var_dump($_REQUEST);DIE;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT' ||
        $_SERVER['REQUEST_METHOD'] == 'POST') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["erro"] = true;
        $response["msg"] = 'O seguintes campos estão faltando ou estão vazios: ' . substr($error_fields, 0, -2);
        echoJSON(400, $response);
        $app->stop();
    }
}