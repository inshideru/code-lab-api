<?php
$app->container->set('ProjetoDTO', function () {
    return new \DataAccess\ProjetoDTO();
});
$app->container->set('ProjetoDAO', function (){
    return new \DataAccess\ProjetoDAO();
});

$app->container->set('Projeto', function($container) {
    return new Controllers\Projeto($container['ProjetoDAO']);
});

$app->container->singleton('PDO', function() {

    $app = \Slim\Slim::getInstance();
    try {
        $pdo = new \PDO(DB_DSN, DB_USER, DB_PASS);
        $pdo->setAttribute(PDO::NULL_EMPTY_STRING, false);
        $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $pdo;

    } catch (\PDOException $e) {
        CodeFail((int)$e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine());
        $app->redirect(SITE_URL);
        return false;
    }

});

$app->container->set('meses', function() {
    return array(
        1 => 'Janeiro',
        2 => 'Fevereiro',
        3 => 'Março',
        4 => 'Abril',
        5 => 'Maio',
        6 => 'Junho',
        7 => 'Julho',
        8 => 'Agosto',
        9 => 'Setembro',
        10 => 'Outubro',
        11 => 'Novembro',
        12 => 'Dezembro'
    );
});

$app->container->set('calendario', function() {
    return (new DateTime())->setTimezone(new DateTimeZone('America/Sao_Paulo'));
});
