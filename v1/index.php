<?php
session_start();

require_once 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

require_once 'Inc/constants.php';
require_once 'Inc/services.php';
require_once 'Inc/init.php';

$app->get('/', function () {
    echo 'Oi!' . SITE_URL;
});

$app->get('/ping', function () use ($app) {
    $response = array('pong' => true);
    echoJSON(200, $response);
});

$app->get('/container/', function () use ($app) {
    var_dump($app->container);
});

$app->get('/hello/:name', function ($name) use ($app) {
    echo $name . ' - ' . $app->container['Projeto']->quem();
});

$app->get('/linguagens', function () use ($app) {
    /** @var  \Controllers\Projeto */
    $projetos = $app->container['Projeto'];
    echoJSON(200, $projetos->getLinguagens());
});

$app->get('/tipos_sistema', function () use ($app) {
    /** @var  \Controllers\Projeto */
    $projetos = $app->container['Projeto'];
    echoJSON(200, $projetos->getTiposSistema());
});

$app->get('/projeto', function () use ($app) {
    /** @var  \Controllers\Projeto */
    $projetos = $app->container['Projeto'];
    echoJSON(200, $projetos->getProjetos());
});

$app->get('/projeto/:id/metrica', function ($id) use ($app) {
    if (!$response = $app->container['Projeto']->getMetrica($id)) {
        $response = array(
            'erro' => true,
            'msg' => 'Projeto não encontrado'
        );
    }
    $response['request_uri'] = $app->request()->getPath();
    echoJSON(200, $response);
});

$app->get('/projeto/:id/pontos_funcao', function ($id) use ($app) {
    if (!$response = $app->container['Projeto']->getPontosFuncao($id)) {
        $response = array(
            'erro' => true,
            'msg' => 'Projeto não encontrado'
        );
    }
    $response['request_uri'] = $app->request()->getPath();
    echoJSON(200, $response);
});


$app->get('/projeto/:id', function ($id) use ($app) {

    if (!$response = $app->container['Projeto']->getProjeto($id)) {
        $response = array(
            'erro' => true,
            'msg' => 'Projeto não encontrado'
        );
    }
    $response['request_uri'] = $app->request()->getPath();
    echoJSON(200, $response);
});

$app->post('/projeto', 'checkContetType', function () use ($app) {
    verifyRequiredParams(array(
        'nome_projeto',
        'modelo',
        'nivel_influencia',
        'id_linguagem',
        'id_tipo_sistema',
        'valor_hora'
    ));

    $request_body = array();
    parse_str($array = $app->request->getBody(), $request_body);

    $response = $app->container['Projeto']->salvarProjeto($request_body);
    echoJSON(201, $response);
});

$app->put('/projeto/:id', 'checkContetType', function ($id) use ($app) {
    verifyRequiredParams(array(
        'nome_projeto',
        'modelo',
        'nivel_influencia',
        'id_linguagem',
        'id_tipo_sistema',
        'valor_hora'
    ));

    $request_body = array();
    /** @var \Controllers\Projeto $ProjetoController */
    $ProjetoController = $app->container['Projeto'];
    parse_str($array = $app->request->getBody(), $request_body);
    $request_body['id_projeto'] = (int)$id;
    if (!$ProjetoController->findProjeto($id)) {
        $response = array(
            'erro' => true,
            'msg' => 'Projeto não encontrado'
        );
        echoJSON(404, $response);
        $app->stop();
    }

    $response = $ProjetoController->salvarProjeto($request_body);
    echoJSON(200, $response);
});

$app->delete('/projeto/:id', function ($id) use ($app) {

    $request_body = array();
    $request_body['id_projeto'] = (int)$id;
    /** @var \Controllers\Projeto $ProjetoController */
    $ProjetoController = $app->container['Projeto'];

    if (!$projeto = $ProjetoController->findProjeto($id)) {
        $response = array(
            'erro' => true,
            'msg' => 'Projeto não encontrado'
        );
        echoJSON(404, $response);
        $app->stop();
    }

    $response = $app->container['Projeto']->removerProjeto($id);
    echoJSON(200, $response);
});


$app->run();